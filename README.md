# zvavwordle - A simple CLI wordle clone

This is a simple wordle clone.  Run the program and give your guesses
on one line each.  `#` marks correct place (green in wordle), `!`
marks correct letter (yellow in wordle) and '.' marks wrong letter
(white in wordle).

Written in response to [NYT's frivolous takedown
notices](https://www.404media.co/nytimes-files-copyright-takedowns-against-hundreds-of-wordle-clones/).
