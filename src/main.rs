/*
    zvavwordle – A simple CLI wordle clone
    Copyright (C) 2024  Matthias Kaak

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

//! # zvavwordle - A simple CLI wordle clone
//!
//! This is a simple wordle clone.  Run the program and give your
//! guesses on one line each.  `#` marks correct place (green in
//! wordle), `!` marks correct letter (yellow in wordle) and '.' marks
//! wrong letter (white in wordle).
//!
//! Written in response to [NYT's frivolous takedown
//! notices](https://www.404media.co/nytimes-files-copyright-takedowns-against-hundreds-of-wordle-clones/).

use std::io::stdin;

use rand::{seq::SliceRandom, thread_rng};

static WORDS: [&[u8; 5]; 2] = [b"abcde", b"qwert"];

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum State
{
    Green,
    Yellow,
    White,
}

fn main()
{
    let mut rng = thread_rng();

    loop
    {
        let word = WORDS.choose(&mut rng).unwrap();

        for line in stdin().lines()
        {
            let line = line.expect("Couldn't read line").as_bytes().to_owned();

            if line.len() != 5 || line.iter().any(|c| !c.is_ascii_lowercase())
            {
                println!("Please give a five (ASCII-)letter word.");
            }

            let mut state = [
                State::White,
                State::White,
                State::White,
                State::White,
                State::White,
            ];

            for i in 0..5
            {
                if line[i] == word[i]
                {
                    state[i] = State::Green;
                }
            }

            let mut happened_already = [0; 26];

            for i in 0..5
            {
                if word
                    .iter()
                    .enumerate()
                    .filter(|(_, c)| **c == line[i])
                    .filter(|(i_, _)| state[*i_] != State::Green)
                    .count()
                    > happened_already[(line[i] - b'a') as usize]
                {
                    state[i] = State::Yellow;
                    happened_already[(line[i] - b'a') as usize] += 1;
                }
            }

            for state in state
            {
                match state
                {
                    State::Green => print!("#"),
                    State::Yellow => print!("!"),
                    State::White => print!("."),
                }
            }
            println!();

            if state.iter().all(|&state| state == State::Green)
            {
                println!("Correct!");
                break;
            }
        }
    }
}
